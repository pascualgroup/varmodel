//
//  Infection.cpp
//  malariamodel
//
//  Created by Ed Baskerville on 5/5/14.
//  Copyright (c) 2014 Ed Baskerville. All rights reserved.
//

#include "Infection.h"
#include "SimParameters.h"
#include "Host.h"
#include <sstream>
#include <algorithm>

using namespace std;

Infection::Infection(Host * hostPtr, int64_t id, StrainPtr & strainPtr, int64_t initialGeneIndex, double initialTime) :
    hostPtr(hostPtr), id(id), strainPtr(strainPtr),
    geneIndex(initialGeneIndex), active(false),
    transitionTime(initialTime)
{
}

void Infection::prepareToEnd()
{
    hostPtr->removeEvent(transitionEvent.get());
    hostPtr->removeEvent(clearanceEvent.get());
}

GenePtr Infection::getCurrentGene()
{
    assert(geneIndex != WAITING_STAGE);
    return strainPtr->getGene(geneIndex);
}

int64_t Infection::getCurrentGeneId()
{
    return getCurrentGene()->id;
}

bool Infection::isImmune()
{
    return hostPtr->immunity.isImmune(getCurrentGene());
}

bool Infection::isClinicallyImmune()
{
    return hostPtr->clinicalImmunity.isImmune(getCurrentGene());
}

double Infection::getTransitionTime()
{
    return transitionTime;
}

double Infection::getAgeAtTransitionTime()
{
    return transitionTime - hostPtr->birthTime;
}

void Infection::performTransition()
{
    transitionTime = hostPtr->getTime();
    
    bool shouldUpdateAllInfections = transitionAffectsAllInfections();
    
    if(geneIndex == WAITING_STAGE) {
        assert(!active);
        geneIndex = 0;
    }
    else if(active) {
        assert(geneIndex != strainPtr->size() - 1);
        GenePtr genePtr = strainPtr->getGene(geneIndex);
        hostPtr->immunity.gainImmunity(genePtr);
        if(hostPtr->getSimulationParametersPtr()->trackClinicalImmunity) {
            hostPtr->clinicalImmunity.gainImmunity(genePtr);
        }
        geneIndex++;
        active = false;
    }
    else {
        active = true;
    }
    
    if(shouldUpdateAllInfections) {
        // Every infection's rates need to be updated
        hostPtr->updateInfectionRates();
    }
    else {
        updateTransitionRate();
        updateClearanceRate();
    }
//    cerr << transitionTime << ": " << toString() << " transitioned to " << geneIndex << "(" << getCurrentGene()->toString() << "), " << (active ? "active" : "not yet active") << '\n';
}

void Infection::updateTransitionRate()
{
    hostPtr->setEventRate(transitionEvent.get(), transitionRate());
}

bool Infection::transitionAffectsAllInfections()
{
    if(geneIndex == WAITING_STAGE) {
        return false;
    }
    else {
        return true;
    }
}

double Infection::transitionRate()
{
    assert(geneIndex != WAITING_STAGE);
    
    if(!active) {
        return activationRate();
    }
    else {
        return deactivationRate();
    }
}

void Infection::updateClearanceRate()
{
    hostPtr->setEventRate(clearanceEvent.get(), clearanceRate());
}

double Infection::activationRate()
{
    assert(!active);
    GenePtr genePtr = getCurrentGene();
    int64_t geneId = getCurrentGeneId();
//    bool immune = isImmune();
//    bool clinicallyImmune = isClinicallyImmune();
//    double age = getAgeAtTransitionTime();
//    int64_t activeCount = hostPtr->getActiveInfectionCount();
//    vector<GenePtr> activeGenes = hostPtr->getActiveInfectionGenes();
//    vector<int64_t> activeGeneIds = hostPtr->getActiveInfectionGeneIds();
//    int64_t immunityCount = hostPtr->getActiveInfectionImmunityCount();
//    int64_t clinicalImmunityCount = hostPtr->getActiveInfectionClinicalImmunityCount();
    
    SimParameters * simParPtr = hostPtr->getSimulationParametersPtr();
    
    double C = simParPtr->genes.baselineActivationTime;
    double p1;
    if(simParPtr->genes.activationPower.size() == 1) {
        p1 = simParPtr->genes.activationPower[0];
    }
    else {
        p1 = simParPtr->genes.activationPower[geneId];
    }
    double nActiveInfections = hostPtr->getActiveInfectionCount();
    
    return pow(nActiveInfections + 1.0, -p1) / C;
}

double Infection::deactivationRate()
{
    assert(active);
    GenePtr genePtr = getCurrentGene();
    int64_t geneId = getCurrentGeneId();
    
    SimParameters * simParPtr = hostPtr->getSimulationParametersPtr();
    
    double d_i;
    if(simParPtr->genes.expressionTime.size() == 1) {
        d_i = simParPtr->genes.expressionTime[0];
    }
    else {
        d_i = simParPtr->genes.expressionTime[geneId];
    }
    
    double f1_i = hostPtr->getEpitopeImmunityCount(genePtr);
    double a_i_sum = hostPtr->getEpitopeCount(genePtr);
    
    if(a_i_sum > 1 && simParPtr->genes.maxExpressionTime.present()) {
        double D = simParPtr->genes.maxExpressionTime;
        return 1.0 / (
            d_i + (D - d_i) * f1_i / (a_i_sum - 1.0)
        );
    }
    return d_i;
}

double Infection::clearanceRate()
{
    SimParameters * simParPtr = hostPtr->getSimulationParametersPtr();
    
    // Liver stage
    if(geneIndex == WAITING_STAGE) {
        return 0.0;
    }
    // Gene active: clearance rate depends on immunity
    else if(active) {
        double clearanceRatePower = simParPtr->withinHost.clearanceRatePower;
        assert(!std::isnan(clearanceRatePower));
        assert(!std::isinf(clearanceRatePower));
        
        double nActiveInfections = hostPtr->getActiveInfectionCount();
        double clearanceRateConstant;
        if(isImmune()) {
            clearanceRateConstant = simParPtr->withinHost.clearanceRateConstantImmune;
        }
        else {
            clearanceRateConstant = simParPtr->withinHost.clearanceRateConstantNotImmune;
        }
        assert(!std::isnan(clearanceRateConstant));
        assert(!std::isinf(clearanceRateConstant));
        assert(clearanceRateConstant > 0.0);
        
        return clearanceRateConstant * std::pow(nActiveInfections, clearanceRatePower);
    }
    // Gene inactive: clearance rate = 0
    else {
        return 0.0;
    }
}

bool Infection::isActive()
{
    return active;
}

double Infection::transmissionProbability()
{
    assert(active);
    
    SimParameters * simParPtr = hostPtr->getSimulationParametersPtr();
    
    GenePtr genePtr = getCurrentGene();
    int64_t geneId = getCurrentGeneId();
    
    double t_i = genePtr->transmissibility;
    
    double f1_i = hostPtr->getEpitopeImmunityCount(genePtr);
    double a_i_sum = hostPtr->getEpitopeCount(genePtr);
    
    double n = hostPtr->getActiveInfectionCount();
    
    double p2;
    if(simParPtr->genes.transmissibilityPower.size() == 1) {
        p2 = simParPtr->genes.transmissibilityPower[0];
    }
    else {
        p2 = simParPtr->genes.transmissibilityPower[geneId];
    }
    
    if(a_i_sum > 1.0 && simParPtr->genes.maxTransmissibility.present()) {
        double T = simParPtr->genes.maxTransmissibility;
        return (t_i + (T - t_i) * f1_i / (a_i_sum - 1.0)) * pow(n, p2);
    }
    
    return t_i * pow(n, p2);
}

std::string Infection::toString()
{
    stringstream ss;
    ss << hostPtr->toString() << ".i" << id;
    return ss.str();
}

void Infection::write(Database & db, Table<InfectionRow> & table)
{
    InfectionRow row;
    row.time = hostPtr->getTime();
    row.hostId = hostPtr->id;
    row.infectionId = id;
    row.strainId = strainPtr->id;
    if(geneIndex == WAITING_STAGE) {
        row.geneIndex.setNull();
        row.active.setNull();
    }
    else {
        row.geneIndex = geneIndex;
        row.active = active;
    }
    db.insert(table, row);
}

void Infection::write(int64_t transmissionId, Database & db, Table<TransmissionInfectionRow> & table)
{
    TransmissionInfectionRow row;
    row.transmissionId = transmissionId;
    row.hostId = hostPtr->id;
    row.infectionId = id;
    row.strainId = strainPtr->id;
    if(geneIndex == WAITING_STAGE) {
        row.geneIndex.setNull();
        row.active.setNull();
    }
    else {
        row.geneIndex = geneIndex;
        row.active = active;
    }
    db.insert(table, row);
}

/*** INFECTION PROCESS EVENTS ***/

InfectionProcessEvent::InfectionProcessEvent(
    std::list<Infection>::iterator infectionItr, double time
) :
    RateEvent(time), infectionItr(infectionItr)
{
}

InfectionProcessEvent::InfectionProcessEvent(
    std::list<Infection>::iterator infectionItr, double rate, double time, zppsim::rng_t & rng
) :
    RateEvent(rate, time, rng), infectionItr(infectionItr)
{
}

TransitionEvent::TransitionEvent(
    std::list<Infection>::iterator infectionItr, double time
) :
    InfectionProcessEvent(infectionItr, time)
{
}


TransitionEvent::TransitionEvent(
    std::list<Infection>::iterator infectionItr, double rate, double time, zppsim::rng_t & rng
) :
    InfectionProcessEvent(infectionItr, rate, time, rng)
{
}

ClearanceEvent::ClearanceEvent(
    std::list<Infection>::iterator infectionItr, double rate, double time, zppsim::rng_t & rng
) :
    InfectionProcessEvent(infectionItr, rate, time, rng)
{
}

void TransitionEvent::performEvent(zppsim::EventQueue &queue)
{
    // If this is the final deactivation, then it's equivalent to clearing
    if(infectionItr->active
        && infectionItr->geneIndex == infectionItr->strainPtr->size() - 1
    ) {
        infectionItr->hostPtr->clearInfection(infectionItr);
    }
    // Otherwise, actually perform a transition
    else {
        infectionItr->performTransition();
    }
}

void ClearanceEvent::performEvent(zppsim::EventQueue &queue)
{
    infectionItr->hostPtr->clearInfection(infectionItr);
}

